-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 19, 2019 at 07:18 AM
-- Server version: 5.6.43
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `youthmon_booksys`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `room_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `time_from` datetime DEFAULT NULL,
  `time_to` datetime DEFAULT NULL,
  `extra_info` text COLLATE utf8mb4_unicode_ci,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `room_id`, `course_id`, `user_id`, `time_from`, `time_to`, `extra_info`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 2, '2019-07-20 10:10:00', '2019-07-20 11:10:00', NULL, 1, '2019-07-18 10:36:29', '2019-07-24 22:31:06', '2019-07-24 22:31:06'),
(2, 2, 2, 10, '2019-07-21 10:10:00', '2019-07-21 11:10:00', NULL, 1, '2019-07-21 08:31:33', '2019-07-24 22:31:06', '2019-07-24 22:31:06'),
(3, 2, 3, 2, '2019-07-28 10:10:00', '2019-07-28 11:10:00', NULL, 1, '2019-07-21 16:51:01', '2019-07-24 22:31:06', '2019-07-24 22:31:06'),
(4, 3, 2, 1, '2019-07-22 10:10:00', '2019-07-22 11:10:00', NULL, 0, '2019-07-21 21:36:11', '2019-07-24 22:31:06', '2019-07-24 22:31:06'),
(5, 1, 2, 1, '2019-07-27 10:10:00', '2019-07-27 11:10:00', NULL, 0, '2019-07-22 10:02:51', '2019-07-24 22:31:06', '2019-07-24 22:31:06'),
(6, 7, 2, 11, '2019-07-27 20:19:00', '2019-07-27 21:30:00', NULL, 0, '2019-07-26 20:25:19', '2019-07-26 20:25:19', NULL),
(7, 7, 2, 2, '2019-08-03 08:30:00', '2019-08-03 10:00:00', 'i want to book this room', 1, '2019-07-27 21:08:36', '2019-07-27 21:09:53', NULL),
(8, 9, 2, 2, '2019-08-06 08:30:00', '2019-08-06 10:00:00', NULL, 1, '2019-07-31 22:58:03', '2019-08-01 15:36:45', NULL),
(9, 20, 8, 2, '2019-08-06 04:00:00', '2019-08-06 05:30:00', NULL, 0, '2019-08-03 20:26:10', '2019-08-03 20:46:32', '2019-08-03 20:46:32'),
(10, 21, 8, 2, '2019-08-04 03:00:00', '2019-08-04 05:00:00', NULL, 0, '2019-08-03 20:45:08', '2019-08-03 20:46:41', '2019-08-03 20:46:41'),
(11, 22, 8, 2, '2019-08-04 08:30:00', '2019-08-04 09:30:00', NULL, 1, '2019-08-03 21:15:43', '2019-08-03 21:16:30', NULL),
(12, 23, 8, 2, '2019-08-06 08:30:00', '2019-08-06 10:00:00', NULL, 2, '2019-08-03 21:36:00', '2019-09-10 23:08:26', NULL),
(13, 10, 6, 1, '2019-08-25 03:00:00', '2019-08-25 04:30:00', NULL, 1, '2019-08-21 10:55:52', '2019-08-21 12:43:50', NULL),
(14, 19, 1, 1, '2019-09-08 01:00:00', '2019-09-06 02:30:00', NULL, 0, '2019-09-06 11:01:39', '2019-09-06 11:01:39', NULL),
(15, 10, 6, 1, '2019-09-08 03:00:00', '2019-09-06 04:30:00', NULL, 1, '2019-09-06 23:02:34', '2019-09-06 23:02:46', NULL),
(16, 16, 6, 1, '2019-09-16 08:30:00', '2019-09-16 10:30:00', NULL, 2, '2019-09-10 21:05:38', '2019-09-10 21:17:11', '2019-09-10 21:17:11');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `teacher_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `teacher_id`, `code`, `name`, `created_at`, `updated_at`) VALUES
(1, 6, '101', 'Network Security', '2019-07-18 08:55:17', '2019-07-21 08:23:26'),
(2, 6, '205', 'Data Structure', '2019-07-18 08:55:17', '2019-07-21 08:23:13'),
(3, 6, '304', 'Database Management', '2019-07-18 08:55:17', '2019-07-21 08:23:18'),
(6, 13, 'CSE-212', 'Digital Electronics', '2019-07-31 23:08:39', '2019-09-10 21:02:41'),
(7, 8, 'CSE234', 'Numerical Methods', '2019-07-31 23:09:40', '2019-07-31 23:09:40'),
(8, 12, 'CSE313', 'Computer Networks', '2019-07-31 23:10:08', '2019-08-03 10:39:41'),
(9, 6, 'CSE-444', 'Robotics', '2019-09-10 21:01:12', '2019-09-10 21:01:12'),
(10, 16, 'CSE-212', 'Digital Electronics', '2019-09-10 21:03:36', '2019-09-10 21:03:36');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'CSE', '2019-07-18 08:55:17', '2019-07-18 08:55:17'),
(2, 'IIT', '2019-07-18 08:55:17', '2019-07-18 08:55:17'),
(3, 'EEE', '2019-07-18 08:55:17', '2019-07-18 08:55:17'),
(4, 'Software Engineering', '2019-07-28 08:38:22', '2019-07-28 08:38:22'),
(5, 'Pharmacy', '2019-07-28 08:39:00', '2019-07-28 08:39:00'),
(6, 'Civil Engineering', '2019-07-28 08:39:23', '2019-07-28 08:39:23'),
(7, 'Architecture', '2019-07-28 08:39:36', '2019-07-28 08:39:36'),
(8, 'English', '2019-07-28 08:39:54', '2019-07-28 08:39:54'),
(9, 'Public Health', '2019-07-28 08:40:11', '2019-07-28 08:40:11');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(33, '2019_06_30_100000_create_password_resets_table', 1),
(34, '2019_06_30_143908_create_departments_table', 1),
(35, '2019_06_30_191448_create_roles_table', 1),
(36, '2019_06_30_191453_create_users_table', 1),
(37, '2019_06_30_192145_create_rooms_table', 1),
(38, '2019_07_02_095245_create_user_details_table', 1),
(39, '2019_07_03_070732_create_teachers_table', 1),
(40, '2019_07_03_070840_create_courses_table', 1),
(41, '2019_07_04_082345_create_bookings_table', 1),
(42, '2019_07_17_123830_create_room_avails_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ashraful@gmail.com', '$2y$10$c5PBndUX/P9Ex9cb8E1O3uEjDnKd8pn3LPza3nws8ZlwIx8DPtidW', '2019-08-03 21:31:02');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Administrator (department)', '2019-07-18 08:55:17', '2019-07-18 08:55:17'),
(2, 'Student (CR)', '2019-07-18 08:55:17', '2019-07-18 08:55:17'),
(3, 'Teacher', '2019-07-21 05:56:03', '2019-07-21 05:56:03');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `room_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `floor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `time_from` datetime DEFAULT NULL,
  `time_to` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room_number`, `floor`, `description`, `time_from`, `time_to`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'L-101', 'CSE', NULL, NULL, NULL, '2019-07-18 08:55:17', '2019-07-24 22:31:26', '2019-07-24 22:31:26'),
(2, 'L-902', 'CSE', NULL, NULL, NULL, '2019-07-18 08:55:17', '2019-07-24 22:31:26', '2019-07-24 22:31:26'),
(3, 'L-902', 'IIT', NULL, NULL, NULL, '2019-07-18 08:55:17', '2019-07-24 22:31:26', '2019-07-24 22:31:26'),
(4, 'CSE103', '1', NULL, NULL, NULL, '2019-07-22 09:06:36', '2019-07-24 22:31:26', '2019-07-24 22:31:26'),
(5, 'CSE902', '9th', NULL, NULL, NULL, '2019-07-22 09:10:09', '2019-07-24 22:31:26', '2019-07-24 22:31:26'),
(6, '903', 'DT-5', NULL, NULL, NULL, '2019-07-22 22:37:10', '2019-07-24 22:31:26', '2019-07-24 22:31:26'),
(7, '903', 'CSE', NULL, NULL, NULL, '2019-07-26 20:19:46', '2019-07-27 21:03:54', NULL),
(8, '103', 'DT-5', NULL, NULL, NULL, '2019-07-27 21:05:32', '2019-07-27 21:05:32', NULL),
(9, '303', 'DT-5', NULL, NULL, NULL, '2019-07-31 20:40:27', '2019-07-31 20:40:27', NULL),
(10, '602', 'CSE', NULL, NULL, NULL, '2019-07-31 20:42:32', '2019-07-31 20:42:32', NULL),
(11, '702', 'CSE', NULL, NULL, NULL, '2019-08-01 22:18:36', '2019-08-01 22:18:36', NULL),
(12, '702', 'CSE', NULL, NULL, NULL, '2019-08-01 22:55:23', '2019-08-01 22:55:32', '2019-08-01 22:55:32'),
(13, '804', 'DT-5', NULL, NULL, NULL, '2019-08-02 11:31:07', '2019-08-02 11:31:07', NULL),
(14, '801', 'DT-5', NULL, NULL, NULL, '2019-08-02 11:35:15', '2019-08-02 11:35:15', NULL),
(15, '902', 'CSE', NULL, NULL, NULL, '2019-08-02 11:36:56', '2019-08-02 11:36:56', NULL),
(16, '303', 'DT-5', NULL, NULL, NULL, '2019-08-02 11:40:01', '2019-08-02 11:40:01', NULL),
(17, '305', 'DT-5', NULL, NULL, NULL, '2019-08-03 10:10:59', '2019-08-03 10:10:59', NULL),
(18, '901', 'DT-5', NULL, NULL, NULL, '2019-08-03 10:14:47', '2019-08-03 10:14:47', NULL),
(19, '303', 'DT-5', NULL, NULL, NULL, '2019-08-03 20:13:47', '2019-08-03 20:13:47', NULL),
(20, '402', 'DT-5', NULL, NULL, NULL, '2019-08-03 20:22:16', '2019-08-03 20:22:16', NULL),
(21, '903', 'CSE', NULL, NULL, NULL, '2019-08-03 20:42:12', '2019-08-03 20:42:12', NULL),
(22, '602', 'CSE', NULL, NULL, NULL, '2019-08-03 21:13:55', '2019-08-03 21:13:55', NULL),
(23, '103', 'DT-5', NULL, NULL, NULL, '2019-08-03 21:33:57', '2019-08-03 21:33:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `room_avails`
--

CREATE TABLE `room_avails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `room_id` int(10) UNSIGNED NOT NULL,
  `day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_avails`
--

INSERT INTO `room_avails` (`id`, `room_id`, `day`, `time_from`, `time_to`) VALUES
(1, 1, '0', '10:10:00', '11:10:00'),
(2, 2, '1', '10:10:00', '11:10:00'),
(3, 3, '2', '10:10:00', '11:10:00'),
(4, 1, '0', '12:20:00', '13:20:00'),
(5, 2, '0', '12:20:00', '13:20:00'),
(6, 4, '0', '09:05:00', '10:14:00'),
(7, 4, '0', '09:06:00', '10:06:00'),
(8, 5, '3', '08:09:00', '11:10:00'),
(9, 6, '4', '06:30:00', '07:30:00'),
(10, 6, '4', '04:30:00', '05:30:00'),
(11, 7, '0', '08:30:00', '10:00:00'),
(12, 8, '3', '01:00:00', '02:30:00'),
(13, 9, '3', '08:30:00', '10:00:00'),
(14, 9, '5', '00:00:00', '01:30:00'),
(15, 10, '1', '03:00:00', '04:30:00'),
(16, 10, '2', '10:00:00', '11:30:00'),
(17, 11, '1', '01:00:00', '02:30:00'),
(18, 12, '1', '01:00:00', '02:30:00'),
(19, 13, '2', '11:30:00', '01:00:00'),
(20, 13, '5', '01:00:00', '02:30:00'),
(21, 14, '0', '11:30:00', '01:00:00'),
(22, 15, '0', '03:30:00', '05:00:00'),
(23, 16, '2', '08:30:00', '10:30:00'),
(24, 16, '0', '01:30:00', '03:30:00'),
(25, 17, '1', '01:00:00', '02:30:00'),
(26, 18, '0', '03:30:00', '05:00:00'),
(27, 18, '1', '10:00:00', '11:30:00'),
(28, 19, '1', '03:00:00', '04:30:00'),
(29, 19, '1', '01:00:00', '02:30:00'),
(30, 20, '3', '04:00:00', '05:30:00'),
(31, 21, '1', '03:00:00', '05:00:00'),
(32, 22, '1', '08:30:00', '09:30:00'),
(33, 23, '3', '08:30:00', '10:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desig` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `user_id`, `name`, `desig`, `created_at`, `updated_at`) VALUES
(6, 10, 'R Hossain', 'Professor', '2019-07-21 08:23:03', '2019-07-21 08:23:03'),
(7, 12, 'Md. Sazzadur Ahamed', 'senior lecturer', '2019-07-21 21:38:10', '2019-07-24 22:26:53'),
(8, 17, 'Ms. Rifat Ara Shams', 'Assistant Professor', '2019-07-22 20:01:05', '2019-07-28 08:33:21'),
(10, 16, 'Mr. Md. Sadekur Rahman', 'Assistant Professor', '2019-07-28 08:34:59', '2019-07-28 08:34:59'),
(11, 15, 'Moushumi Zaman Bonny', 'Senior lecturer', '2019-07-28 08:36:34', '2019-07-28 08:36:34'),
(12, 19, 'Mr. Raja Tariqul Hasan Tusher', 'senior lecturer', '2019-07-28 08:44:21', '2019-07-28 08:44:21'),
(13, 20, 'Ms. Farhana Irin', 'Lecturer', '2019-07-28 08:45:34', '2019-07-28 08:45:34'),
(14, 21, 'Mr. Md. Al Maruf', 'Lecturer', '2019-07-28 08:47:16', '2019-07-28 08:47:16'),
(15, 22, 'Ms. Refath Ara Hossain', 'Lecturer', '2019-07-28 08:48:19', '2019-07-28 08:48:19'),
(16, 23, 'rabbi', 'Lecturer', '2019-09-10 21:00:14', '2019-09-10 21:00:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `department_id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Ashraful Haque', 'ashraful@gmail.com', '$2y$10$/JgTYTGJkQxEHO73Adnx8OQjD62DoXCvBTcZ3wzQS.4S.OX/lzHni', 'siwio3xU0cUalRVI1l7y2ZyZ3QkcQW5bafCmjIUqfO5uH5AKSWZxoNNE0HsZ', '2019-07-18 08:55:18', '2019-07-21 17:13:10'),
(2, 2, 1, 'Emon Ahmed', 'emon@gmail.com', '$2y$10$/KrUE/MDZ2My8B7Kxc/aF.n8jnW5fmfYv..LEspJ9.B72Hhdv40.W', NULL, '2019-07-18 08:55:18', '2019-07-21 16:57:30'),
(10, 3, 2, 'R Hossain', 'rhossain.php@gmail.com', '$2y$10$UG26ZMqUESOlwEcU9WLuaep1VkG6gAl/S3Z0HZpAqEzY7ZPvnzII.', NULL, '2019-07-21 08:23:03', '2019-07-21 08:23:03'),
(11, 1, 2, 'Rajib Hossain', 'rajibhossain.php@gmail.com', '$2y$10$t1/XptkWhVx/IUl5T8/De.B0Qi9bswoakFDSkr7LqlYYd91o1BXay', NULL, '2019-07-21 17:21:00', '2019-07-21 17:21:00'),
(12, 3, 1, 'Md. Sazzadur Ahamed', 'sazzad.cse@diu.edu.bd', '$2y$10$.FzuLCsThzYkRwoTEC59VO9ryqr6qqd0d.tICa24T55pb3lmXCDfC', NULL, '2019-07-21 20:00:12', '2019-07-24 22:26:53'),
(15, 3, 1, 'Moushumi Zaman Bonny', 'bonny.cse@diu.edu.bd', '$2y$10$s0C3NjRShHt7.nf7rDpREOeG3uwkZCqOBzDtYNEpHjcZVPfGbtGYy', NULL, '2019-07-21 21:52:43', '2019-07-28 08:36:34'),
(16, 3, 1, 'Mr. Md. Sadekur Rahman', 'sadekur.cse@daffodilvarsity.edu.bd', '$2y$10$NMPXIZBNn/JPx58pJL4tAePHwiynXe4Gvl24w5zTfV3uxc4df5ReC', NULL, '2019-07-21 21:55:42', '2019-07-28 08:34:59'),
(17, 3, 1, 'Ms. Rifat Ara Shams', 'rifat.cse@diu.edu.bd', '$2y$10$A4ZbJNufcx/9XrNuwK.Lt.TCCjdqbSrmbjbCXb3lJmMl/h8uI9v3O', NULL, '2019-07-22 20:01:05', '2019-07-28 08:33:21'),
(19, 3, 1, 'Mr. Raja Tariqul Hasan Tusher', 'tusher.cse@diu.edu.bd', '$2y$10$.TuEXTbNI7/HCuGyIbX/IeUrwv3QvD7CgbBpXBhp3hAQ/6tWqDLWS', NULL, '2019-07-28 08:44:21', '2019-07-28 08:44:21'),
(20, 3, 1, 'Ms. Farhana Irin', 'farhana.cse@diu.edu.bd', '$2y$10$Qpa.4J0kU654yTBNV0GZC.bXe0I.SzxL7NHcisSCcMm0p4Hz/EnOW', NULL, '2019-07-28 08:45:34', '2019-07-28 08:45:34'),
(21, 3, 1, 'Mr. Md. Al Maruf', 'maruf.cse@diu.edu.bd', '$2y$10$pwyfV2IPdOYlN7OUvZxB1.fSiJ7WLC6y2E/1QgCZZoDRB3kWfyHJq', NULL, '2019-07-28 08:47:16', '2019-07-28 08:47:16'),
(22, 3, 1, 'Ms. Refath Ara Hossain', 'refath.cse@diu.edu.bd', '$2y$10$64UXc7bAgLrZZvm5KgoVuO/CMvPoAJDr2kh48dhRcYlXX84yNy7Ia', NULL, '2019-07-28 08:48:19', '2019-07-28 08:48:19'),
(23, 3, 1, 'rabbi', 'rabbi@gmail.com', '$2y$10$mLwyrTHNxj8IhxpxbeDJZ.vEiiHS6bmktlMW.a7IgQ6C77Oj6A3DW', NULL, '2019-09-10 21:00:14', '2019-09-10 21:00:14');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `student_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `student_id`, `batch`, `section`, `created_at`, `updated_at`) VALUES
(1, 2, '153-15-6625', '18', 'MCSE', '2019-07-18 08:55:18', '2019-07-21 16:57:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookings_room_id_foreign` (`room_id`),
  ADD KEY `bookings_user_id_foreign` (`user_id`),
  ADD KEY `bookings_deleted_at_index` (`deleted_at`),
  ADD KEY `bookings_course_id_index` (`course_id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courses_teacher_id_foreign` (`teacher_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rooms_deleted_at_index` (`deleted_at`);

--
-- Indexes for table `room_avails`
--
ALTER TABLE `room_avails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_role_id_foreign` (`role_id`),
  ADD KEY `users_department_id_foreign` (`department_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_details_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `room_avails`
--
ALTER TABLE `room_avails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `bookings_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  ADD CONSTRAINT `bookings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_teacher_id_foreign` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
