<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomAvailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('room_avails')) {
            Schema::create('room_avails', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('room_id')->unsigned();
                $table->string('day');
                $table->time('time_from')->nullable();
                $table->time('time_to')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_avails');
    }
}