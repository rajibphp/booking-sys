<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('bookings')) {
            Schema::create('bookings', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('room_id')->unsigned();
                $table->integer('course_id')->unsigned()->index();
                $table->integer('user_id')->unsigned();
                $table->datetime('time_from')->nullable();
                $table->datetime('time_to')->nullable();
                $table->text('extra_info')->nullable();
                $table->smallInteger('status')->default(0);

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('course_id')->references('id')->on('courses')->onDelete('RESTRICT');
                $table->foreign('room_id')->references('id')->on('rooms')->onDelete('restrict');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('bookings');
        Schema::enableForeignKeyConstraints();
    }
}