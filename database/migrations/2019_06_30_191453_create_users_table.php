<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('role_id')->unsigned();
                $table->integer('department_id')->unsigned();
                $table->string('name');
                $table->string('desig')->nullable();
                $table->string('email');
                $table->string('password');
                $table->string('remember_token')->nullable();
                $table->timestamps();

                $table->foreign('role_id')->references('id')->on('roles')->onDelete('restrict');
                $table->foreign('department_id')->references('id')->on('departments')->onDelete('restrict');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}