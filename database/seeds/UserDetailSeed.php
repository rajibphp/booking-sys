<?php

use Illuminate\Database\Seeder;

class UserDetailSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'user_id' => 2, 'student_id' => 18309, 'batch' => 18, 'section' => 'MCSE'],
        ];

        foreach ($items as $item) {
            \App\UserDetail::create($item);
        }
    }
}
