<?php

use Illuminate\Database\Seeder;

class CourseSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'teacher_id' => 1, 'code' => 101, 'name' => 'Network Security'],
            ['id' => 2, 'teacher_id' => 2, 'code' => 205, 'name' => 'Data Structure'],
            ['id' => 3, 'teacher_id' => 2, 'code' => 304, 'name' => 'Database Management'],
        ];

        foreach ($items as $item) {
            \App\Course::create($item);
        }
    }
}