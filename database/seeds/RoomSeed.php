<?php

use Illuminate\Database\Seeder;

class RoomSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'room_number' => 'L-101', 'floor' => 'CSE'],
            ['id' => 2, 'room_number' => 'L-902', 'floor' => 'CSE'],
            ['id' => 3, 'room_number' => 'L-902', 'floor' => 'IIT'],
        ];

        foreach ($items as $item) {
            \App\Room::create($item);
        }
    }
}
