<?php

use Illuminate\Database\Seeder;

class RoomAvailSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'room_id' => 1, 'day' => 0, 'time_from' => '10:10:00', 'time_to' => '10:10:00'],
            ['id' => 2, 'room_id' => 2, 'day' => 1, 'time_from' => '10:10:00', 'time_to' => '10:10:00'],
            ['id' => 3, 'room_id' => 3, 'day' => 2, 'time_from' => '10:10:00', 'time_to' => '10:10:00'],
        ];

        foreach ($items as $item) {
            \App\RoomAvail::create($item);
        }
    }
}
