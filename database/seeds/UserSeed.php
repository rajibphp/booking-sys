<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'name' => 'Rajib Hossain', 'email' => 'rajibhossain.php@gmail.com',
                'password' => \Hash::make('12345678'), 'role_id' => 1, 'department_id' => 1],

            ['id' => 2, 'name' => 'Emon Ahmed', 'email' => 'emon@gmail.com',
                'password' => \Hash::make('12345678'), 'role_id' => 2, 'department_id' => 1],

            ['id' => 3, 'name' => 'Rajib Hossain', 'email' => 'rhossain.php@gmail.com',
                'password' => \Hash::make('12345678'), 'role_id' => 3, 'department_id' => 1],

            ['id' => 4, 'name' => 'Shaibal Hyder', 'email' => 'hyder@gmail.com',
                'password' => \Hash::make('12345678'), 'role_id' => 3, 'department_id' => 1],
        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }

//        factory(App\User::class)->create();
    }
}
