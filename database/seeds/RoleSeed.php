<?php

use Illuminate\Database\Seeder;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'title' => 'Administrator (department)'],
            ['id' => 2, 'title' => 'Student (CR)'],
            ['id' => 3, 'title' => 'Teacher'],
        ];

        foreach ($items as $item) {
            \App\Role::create($item);
        }
    }
}
