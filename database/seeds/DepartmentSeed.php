<?php

use Illuminate\Database\Seeder;

class DepartmentSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'name' => 'CSE'],
            ['id' => 2, 'name' => 'IIT'],
            ['id' => 3, 'name' => 'EEE'],
        ];

        foreach ($items as $item) {
            \App\Department::create($item);
        }
    }
}