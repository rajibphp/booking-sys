<?php

use Illuminate\Database\Seeder;

class TeacherSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'user_id' => 3, 'name' => 'Rajib Hossain', 'desig' => 'Asst. Professor'],
            ['id' => 2, 'user_id' => 4, 'name' => 'Shaibal Hyder', 'desig' => 'Asst. Professor'],
        ];

        foreach ($items as $item) {
            \App\Teacher::create($item);
        }
    }
}