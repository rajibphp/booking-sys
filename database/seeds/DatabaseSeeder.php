<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeed::class);
        $this->call(DepartmentSeed::class);
        $this->call(RoomSeed::class);
        $this->call(RoomAvailSeed::class);
        $this->call(TeacherSeed::class);
        $this->call(CourseSeed::class);
        $this->call(UserSeed::class);
        $this->call(UserDetailSeed::class);
    }
}