@php
    $avail = null;
    if(isset($room)){
        $avail = $room->avail ? : null;
    }
@endphp

<div class="col-md-6 room_avail">
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.room-avail.title')
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <table class="table table-bordered table-striped availTable" id="availTable">
                        <thead>
                        <tr>
                            <th>{{ transFunc('room-avail.fields.day') }}</th>
                            <th>{{ transFunc('bookings.fields.time-from') }}</th>
                            <th>{{ transFunc('bookings.fields.time-to') }}</th>
                            <th>Add/ Remove</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($avail) > 0)
                            @foreach($avail as $row)
                                <tr>
                                    <td>
                                        {!! Form::select('day[]', days(),  $row->day, ['class' => 'form-control select2 required']) !!}
                                        {!! Form::hidden('avail_id[]', $row->id) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('time_from[]', timeFormat('g:i a', $row->time_from), ['class' => 'form-control datetimepicker', 'required']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('time_to[]', timeFormat('g:i a', $row->time_to), ['class' => 'form-control datetimepicker', 'required']) !!}
                                    </td>
                                    <td>
                                    @if($loop->iteration == 1)
                                        {!! Form::button('Add More', ['class' => 'btn btn-default addRow']) !!}
                                    @else
                                        {!! Form::button('Remove', ['class' => 'btn btn-danger deleteRow']); !!}
                                    @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>
                                    {!! Form::select('day[]', days(),  old('day'), ['class' => 'form-control select2 required']) !!}
                                </td>
                                <td>
                                    {!! Form::text('time_from[]', old('time_from'), ['class' => 'form-control datetimepicker', 'required']) !!}
                                </td>
                                <td>
                                    {!! Form::text('time_to[]', old('time_to'), ['class' => 'form-control datetimepicker', 'required']) !!}
                                </td>
                                <td>
                                    {!! Form::button('Add More', ['class' => 'btn btn-default addRow']) !!}
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

@section('javascript')
    @parent
    <script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $('.datetimepicker').datetimepicker({
            format: "hh:mm a"
        });

        var sl = 1;
        $(".addRow").click(function () {
            sl++;
            var day = '<?= Form::select('day[]', days(), old('day'), ['class' => 'form-control select2 required']); ?>';
            var time_from = '<?= Form::text('time_from[]', old('time_from'), ['class' => 'form-control datetimepicker', 'required']); ?>';
            var time_to = '<?= Form::text('time_to[]', old('time_to'), ['class' => 'form-control datetimepicker', 'required']); ?>';
            var deleteRow = '<?= Form::button('Remove', ['class' => 'btn btn-danger deleteRow']); ?>';
            var markup = '<tr id="tr_' + sl + '"><td>' + day + '</td><td>' + time_from + '</td><td>' + time_to + '</td><td>' + deleteRow + '</td></tr>';
            $("table tbody").append(markup);
            $('.datetimepicker').datetimepicker({
                format: "hh:mm a"
            });
        });

        $('#availTable').on('click', '.deleteRow', function () {
            $(this).closest('tr').remove();
        });
    </script>
@stop