<div class="panel-body">
    <div class="row">
        <div class="col-xs-12 form-group">
            {!! Form::label('room_number', transFunc('rooms.fields.room-number').'*', ['class' => 'control-label']) !!}
            {!! Form::text('room_number', old('room_number'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
            @include( transFunc('error_block'), ['field_id'=> 'room_number'])
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 form-group">
            {!! Form::label('floor', transFunc('rooms.fields.floor').'*', ['class' => 'control-label']) !!}
            {!! Form::text('floor', old('floor'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
            @include( transFunc('error_block'), ['field_id'=> 'floor'])
        </div>
    </div>
</div>