@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.rooms.title')</h3>

    {!! Form::model($room, ['method' => 'PUT', 'route' => ['rooms.update', $room->id]]) !!}

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('quickadmin.qa_edit')
                </div>

                @include('admin.rooms.input-fields')
            </div>
        </div>
        @include('admin.rooms.roomAvailFields')
    </div>
    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop