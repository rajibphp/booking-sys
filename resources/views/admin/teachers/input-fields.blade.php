<div class="panel-body">
    <div class="row">
        <div class="col-xs-12 form-group">
            {!! Form::label('name', trans('quickadmin.teachers.fields.name').'*', ['class' => 'control-label']) !!}
            {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
            <p class="help-block"></p>
            @if($errors->has('name'))
                <p class="help-block">
                    {{ $errors->first('name') }}
                </p>
            @endif
        </div>

        <div class="col-xs-12 form-group">
            {!! Form::label('desig', trans('quickadmin.teachers.fields.desig').'*', ['class' => 'control-label']) !!}
            {!! Form::text('desig', old('desig'), ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
            <p class="help-block"></p>
            @if($errors->has('desig'))
                <p class="help-block">
                    {{ $errors->first('desig') }}
                </p>
            @endif
        </div>
    </div>

</div>