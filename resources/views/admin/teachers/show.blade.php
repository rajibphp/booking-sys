@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.teachers.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.teachers.fields.name')</th>
                            <td field-key='name'>{{ $teacher->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.teachers.fields.desig')</th>
                            <td field-key='name'>{{ $teacher->desig }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('teachers.index') }}"
               class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
