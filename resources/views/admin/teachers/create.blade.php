@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.teachers.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['teachers.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>
        @include('admin.teachers.input-fields')
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop