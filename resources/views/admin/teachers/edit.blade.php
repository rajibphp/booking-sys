@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.teachers.title')</h3>

    {!! Form::model($teacher, ['method' => 'PUT', 'route' => ['teachers.update', $teacher->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>
        @include('admin.teachers.input-fields')
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop