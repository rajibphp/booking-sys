@if($errors->has($field_id))
    <span class="help-block" style="color: red;">
        {{ $errors->first($field_id) }}
    </span>
@endif