@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.users.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.users.fields.name')</th>
                            <td field-key='name'>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.email')</th>
                            <td field-key='email'>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.role')</th>
                            <td field-key='role'>{{ $user->role->title }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.dept')</th>
                            <td field-key='role'>{{ $user->department->name }}</td>
                        </tr>
                        @if($user->role_id == 3)
                            <tr>
                                <th>@lang('quickadmin.teachers.fields.desig')</th>
                                <td field-key='role'>{{ $user->teacher->desig }}</td>
                            </tr>
                        @endif
                    </table>
                </div>
                @if($user->role_id == 2)
                    <div class="col-md-6">
                        <table class="table table-bordered table-striped">
                            <caption>@lang('quickadmin.user-details.title')</caption>
                            <tr>
                                <th>@lang('quickadmin.user-details.fields.student_id')</th>
                                <td field-key='student_id'>{{ $user->detail->student_id }}</td>
                            </tr>
                            <tr>
                                <th>@lang('quickadmin.user-details.fields.batch')</th>
                                <td field-key='batch'>{{ $user->detail->batch }}</td>
                            </tr>
                            <tr>
                                <th>@lang('quickadmin.user-details.fields.section')</th>
                                <td field-key='section'>{{ $user->detail->section }}</td>
                            </tr>
                        </table>
                    </div>
                @endif
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('users.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop