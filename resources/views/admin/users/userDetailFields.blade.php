@php
    $detail = null;
    $teacher = null;
    if(isset($user)){
        $detail = $user->detail ? : null;
        $teacher = $user->role_id == 3 ? $user->teacher : null;
    }
@endphp
<div class="col-md-6 form-group">
    {!! Form::label('department_id', trans('quickadmin.rooms.fields.category').'*', ['class' => 'control-label']) !!}
    {!! Form::select('department_id', $departments, old('department_id'), ['class' => 'form-control select2']) !!}
    <p class="help-block"></p>
    @if($errors->has('department_id'))
        <p class="help-block">
            {{ $errors->first('department_id') }}
        </p>
    @endif
</div>

<div class="col-md-6 cr_details" {{ $detail ? '' : 'hidden' }}>
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.user-details.title')
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('student_id', trans('quickadmin.user-details.fields.student_id').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('student_id', $detail ? $detail->student_id : old('student_id'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('student_id'))
                        <p class="help-block">
                            {{ $errors->first('student_id') }}
                        </p>
                    @endif
                </div>

                <div class="col-xs-12 form-group">
                    {!! Form::label('batch', trans('quickadmin.user-details.fields.batch').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('batch', $detail ? $detail->batch : old('department_id'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('batch'))
                        <p class="help-block">
                            {{ $errors->first('batch') }}
                        </p>
                    @endif
                </div>

                <div class="col-xs-12 form-group">
                    {!! Form::label('section', trans('quickadmin.user-details.fields.section').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('section', $detail ? $detail->section : old('section'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('section'))
                        <p class="help-block">
                            {{ $errors->first('section') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-6 teacher_details" {{ $teacher ? '' : 'hidden' }}>
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.teachers.fields.desig')
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('desig', trans('quickadmin.teachers.fields.desig').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('desig', $teacher ? $teacher->desig : old('desig'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    @include( transFunc('error_block'), ['field_id'=> 'desig'])
                </div>

            </div>
        </div>
    </div>
</div>