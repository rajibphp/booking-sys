@php
    $disabled = isset($_GET['room_id']) ? 'readonly': '';
@endphp
<div class="panel-body">
    <div class="row">
        <div class="col-xs-12 form-group">
            {!! Form::label('user_id', trans('quickadmin.bookings.fields.customer').'*', ['class' => 'control-label']) !!}
            {!! Form::select('user_id', $users, old('user_id'), ['class' => 'form-control select2 required']) !!}
            @include( transFunc('error_block'), ['field_id'=> 'user_id'])
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 form-group">
            {!! Form::label('course_id', transFunc('bookings.fields.course').'*', ['class' => 'control-label']) !!}
            {!! Form::select('course_id', $courses, old('course_id'), ['class' => 'form-control select2']) !!}
            @include( transFunc('error_block'), ['field_id'=> 'course_id'])
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 form-group">
            {!! Form::label('room_id', trans('quickadmin.bookings.fields.room').'*', ['class' => 'control-label']) !!}
            {!! Form::select('room_id', $rooms, $_GET['room_id'] ?? old('room_id'), ['class' => 'form-control select2']) !!}
            @include( transFunc('error_block'), ['field_id'=> 'room_id'])
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 form-group">
            {!! Form::label('time_from', trans('quickadmin.bookings.fields.time-from').'*', ['class' => 'control-label']) !!}
            {!! Form::text('time_from', $_GET['time_from'] ?? old('time_from'), ['class' => 'form-control datetimepicker', 'required', $disabled]) !!}
            @include( transFunc('error_block'), ['field_id'=> 'time_from'])
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 form-group">
            {!! Form::label('time_to', trans('quickadmin.bookings.fields.time-to').'*', ['class' => 'control-label']) !!}
            {!! Form::text('time_to', $_GET['time_to'] ?? old('time_to'), ['class' => 'form-control datetimepicker', 'required', $disabled]) !!}
            @include( transFunc('error_block'), ['field_id'=> 'time_to'])
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 form-group">
            {!! Form::label('extra_info', trans('quickadmin.bookings.fields.additional-information'), ['class' => 'control-label']) !!}
            {!! Form::textarea('extra_info', old('extra_info'), ['class' => 'form-control ', 'placeholder' => 'Note']) !!}
            @include( transFunc('error_block'), ['field_id'=> 'extra_info'])
        </div>
    </div>

</div>

@section('javascript')
    @parent
    <script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $('.datetimepicker').datetimepicker({
            format: "YYYY-MM-DD hh:mm a"
        });
    </script>
@stop