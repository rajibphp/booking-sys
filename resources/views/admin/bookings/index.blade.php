@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@php
    $admin = \Auth::user()->role_id == 1 ?? null;
@endphp

@section('content')
    <h3 class="page-title">@lang('quickadmin.bookings.title')</h3>
    @can('booking_create22')
        <p>
            <a href="{{ route('bookings.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        </p>
    @endcan

    @can('admin_access')
        <p>
        <ul class="list-inline">
            <li><a href="{{ route('bookings.index') }}"
                   style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a>
            </li>
            |
            <li><a href="{{ route('bookings.index') }}?show_deleted=1"
                   style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a>
            </li>
        </ul>
        </p>
    @endcan

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($bookings) > 0 ? 'datatable' : '' }} @can('admin_access') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                <tr>
                    @can('admin_access')
                        @if ( request('show_deleted') != 1 )
                            <th style="text-align:center;"><input type="checkbox" id="select-all"/></th>@endif
                    @endcan

                    <th>@lang('quickadmin.bookings.fields.customer')</th>
                    <th>{{ transFunc('bookings.fields.course') }}</th>
                    <th>{{ transFunc('courses.fields.teacher') }}</th>
                    <th>@lang('quickadmin.bookings.fields.room')</th>
                    <th>@lang('quickadmin.bookings.fields.time-from')</th>
                    <th>@lang('quickadmin.bookings.fields.time-to')</th>
                    <th>{{ transFunc('bookings.fields.status') }}</th>
                    @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                    @else
                        <th>&nbsp;</th>
                    @endif
                </tr>
                </thead>

                <tbody>
                @if (count($bookings) > 0)
                    @foreach ($bookings as $booking)
                        <tr data-entry-id="{{ $booking->id }}">
                            @can('admin_access')
                                @if ( request('show_deleted') != 1 )
                                    <td></td>@endif
                            @endcan

                            <td field-key='student'>{{ $booking->user->name." [".$booking->user->department->name."]" }}</td>
                            <td field-key='course'>{{ $booking->course->code." [".$booking->course->name."]" }}</td>
                            <td field-key='course'>{{ $booking->course->teacher->name." [".$booking->course->teacher->desig."]" }}</td>
                            <td field-key='room'>{{ $booking->room->room_number." [".$booking->room->floor."]" }}</td>
                            <td field-key='time_from'>{{ $booking->time_from }}</td>
                            <td field-key='time_to'>{{ $booking->time_to }}</td>
                            <td class="{{ statusClass($booking->status) }}">
                                @if($admin)
                                    @can('admin_access')
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PUT',
                                            'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                            'route' => ['bookings.updateStatus', $booking->id]
                                            )) !!}
                                        <span style="background-color: red">{!! Form::select('status', status(), $booking->status, ['class' => 'form-control select2']) !!}</span>
                                        {!! Form::submit(trans('quickadmin.qa_update'), array('class' => 'btn btn-xs btn-success')) !!}
                                        {!! Form::close() !!}
                                    @endcan
                                @else
                                    {!! status()[$booking->status] !!}
                                @endif
                            </td>
                            @if( request('show_deleted') == 1 )
                                <td>
                                    @can('admin_access')
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'POST',
                                            'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                            'route' => ['bookings.restore', $booking->id])) !!}
                                        {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                        {!! Form::close() !!}
                                    @endcan
                                    @can('admin_access')
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'DELETE',
                                            'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                            'route' => ['bookings.perma_del', $booking->id])) !!}
                                        {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                        {!! Form::close() !!}
                                    @endcan
                                </td>
                            @else
                                <td>
                                    @can('booking_view')
                                        <a href="{{ route('bookings.show',[$booking->id]) }}"
                                           class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('booking_edit')
                                        <a href="{{ route('bookings.edit',[$booking->id]) }}"
                                           class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('admin_access')
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'DELETE',
                                            'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                            'route' => ['bookings.destroy', $booking->id])) !!}
                                        {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                        {!! Form::close() !!}
                                    @endcan
                                </td>
                            @endif
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        @can('admin_access')
                @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('bookings.mass_destroy') }}'; @endif
        @endcan
    </script>
@endsection