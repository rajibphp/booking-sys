@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.bookings.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.bookings.fields.customer')</th>
                            <td field-key='customer'>{{ $booking->user->name." [". $booking->user->department->name."]" }}</td>
                        </tr>
                        <tr>
                            <th>{{ transFunc('bookings.fields.course') }}</th>
                            <td field-key='course'>{{ $booking->course->code." [". $booking->course->name."]" }}</td>
                        </tr>
                        <tr>
                            <th>{{ transFunc('courses.fields.teacher') }}</th>
                            <td field-key='course'>{{ $booking->course->teacher->name." [". $booking->course->teacher->desig."]" }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.bookings.fields.room')</th>
                            <td field-key='room'>{{ $booking->room->room_number." [". $booking->room->floor."]" }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.bookings.fields.time-from')</th>
                            <td field-key='time_from'>{{ $booking->time_from }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.bookings.fields.time-to')</th>
                            <td field-key='time_to'>{{ $booking->time_to }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.bookings.fields.additional-information')</th>
                            <td field-key='extra_info'>{!! $booking->extra_info !!}</td>
                        </tr>

                        <tr>
                            <th>@lang('quickadmin.bookings.fields.status')</th>
                            <td field-key='status' class="{{ statusClass($booking->status) }}">{!! status()[$booking->status] !!}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('bookings.index') }}"
               class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop