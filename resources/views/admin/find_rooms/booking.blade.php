<table class="table table-bordered table-striped datatable">
    <thead>
    <tr>
        <th>{{ transFunc('bookings.fields.course') }}</th>
        <th>{{ transFunc('courses.fields.teacher') }}</th>
        <th>@lang('quickadmin.bookings.fields.time-from')</th>
        <th>@lang('quickadmin.bookings.fields.time-to')</th>
        <th>{{ transFunc('bookings.fields.status') }}</th>
        <th></th>
    </tr>
    </thead>

    <tbody>
    @foreach ( $room->booking as $booking)
        <tr data-entry-id="{{ $booking->id }}">
            <td field-key='course'>{{ $booking->course->code." [".$booking->course->name."]" }}</td>
            <td field-key='course'>{{ $booking->course->teacher->name." [".$booking->course->teacher->desig."]" }}</td>
            <td field-key='time_from'>{{ $booking->time_from }}</td>
            <td field-key='time_to'>{{ $booking->time_to }}</td>
            <td>{!! status()[$booking->status] !!}</td>
            <td>
                @can('booking_view')
                    <a href="{{ route('bookings.show',[$booking->id]) }}" target="_blank"
                       class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                @endcan
            </td>
        </tr>
    @endforeach
    </tbody>
</table>