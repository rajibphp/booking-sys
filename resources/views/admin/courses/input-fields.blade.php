<div class="panel-body">
    <div class="row">
        <div class="col-xs-12 form-group">
            {!! Form::label('code', trans('quickadmin.courses.fields.code').'*', ['class' => 'control-label']) !!}
            {!! Form::text('code', old('code'), ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
            <p class="help-block"></p>
            @if($errors->has('code'))
                <p class="help-block">
                    {{ $errors->first('code') }}
                </p>
            @endif
        </div>

        <div class="col-xs-12 form-group">
            {!! Form::label('name', trans('quickadmin.courses.fields.name').'*', ['class' => 'control-label']) !!}
            {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
            <p class="help-block"></p>
            @if($errors->has('name'))
                <p class="help-block">
                    {{ $errors->first('name') }}
                </p>
            @endif
        </div>

        <div class="col-xs-12 form-group">
            {!! Form::label('teacher_id', transFunc('courses.fields.teacher'), ['class' => 'control-label']) !!}
            {!! Form::select('teacher_id', $teachers, old('teacher_id'), ['class' => 'form-control select2']) !!}
            <p class="help-block"></p>
            @if($errors->has('teacher_id'))
                <p class="help-block">
                    {{ $errors->first('teacher_id') }}
                </p>
            @endif
        </div>
    </div>

</div>