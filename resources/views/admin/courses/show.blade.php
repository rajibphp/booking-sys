@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.courses.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.courses.fields.code')</th>
                            <td field-key='name'>{{ $course->code }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.courses.fields.name')</th>
                            <td field-key='name'>{{ $course->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.courses.fields.teacher')</th>
                            <td field-key='name'>{{ $course->teacher->name }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('courses.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
