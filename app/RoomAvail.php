<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomAvail extends Model
{
    protected $fillable = ['room_id', 'day', 'time_from', 'time_to'];
    public $timestamps = false;

    public function room()
    {
        return $this->belongsTo('App\Room');
    }

    /**
     * Set attribute to date format
     * @param $input
     */
//    public function setTimeFromAttribute($input)
//    {
//        $this->attributes['time_from'] = timeFormat('H:i', $input);
//    }
//
//    public function setTimeToAttribute($input)
//    {
//        $this->attributes['time_to'] = timeFormat('H:i', $input);
//    }
}