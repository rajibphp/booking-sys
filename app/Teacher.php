<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = ['name', 'desig'];

    static function selectList()
    {
        $select = ['' => $trans = transFunc()];
        $teachers = self::all();
        foreach ($teachers as $item)
            $select[$item->id] = $item->name . " [" . $item->desig . "]";
        return $select;
    }
}