<?php
function status(): array
{
    return [
        'Pending',
        'Approved',
        'Cancelled',
    ];
}

function days(): array
{
    return [
        'Saturday',
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
    ];
}

function statusClass($status = null)
{
    $class = 'warning';
    if ($status == 1) {
        $class = 'success';
    } elseif ($status == 2) {
        $class = 'danger';
    }
    return $class;
}

// 12-hour time to 24-hour time & vice versa
function convertTime($input = "Y-m-d h:m:s", $format = 12)
{
    return DATE($format == 12 ? "Y-m-d g:i a" : "Y-m-d H:i:s", STRTOTIME($input));
}

//H:i = 24, g:h:i = 12
function timeFormat($format = "H:i", $input)
{
    return ($input != null && $input != '') ? date($format, strtotime($input)) : null;
}

function transFunc($key = 'qa_please_select')
{
    return trans('quickadmin.' . $key);
}

function queryLog()
{
    return \DB::getQueryLog();
}

function redirectException($exception, $type = 'warning')
{
    return redirect()->back()->with($type, $exception->getMessage());
}

function pageTitle($route)
{
    $items = [
        'qa_login' => 'Login',
        '' => 'qa_dashboard',
        'auth.change_password' => 'qa_change_password',

        'roles.index' => 'roles.title',
        'roles.create' => 'roles.title',
        'roles.show' => 'roles.title',
        'roles.edit' => 'roles.title',

        'users.index' => 'users.title',
        'users.create' => 'users.title',
        'users.show' => 'users.title',
        'users.edit' => 'users.title',

        'departments.index' => 'departments.title',
        'departments.create' => 'departments.title',
        'departments.show' => 'departments.title',
        'departments.edit' => 'departments.title',

        'courses.index' => 'courses.title',
        'courses.create' => 'courses.title',
        'courses.show' => 'courses.title',
        'courses.edit' => 'courses.title',

        'teachers.index' => 'teachers.title',
        'teachers.create' => 'teachers.title',
        'teachers.show' => 'teachers.title',
        'teachers.edit' => 'teachers.title',

        'rooms.index' => 'rooms.title',
        'rooms.create' => 'rooms.title',
        'rooms.show' => 'rooms.title',
        'rooms.edit' => 'rooms.title',

        'bookings.index' => 'bookings.title',
        'bookings.create' => 'bookings.title',
        'bookings.show' => 'bookings.title',
        'bookings.edit' => 'bookings.title',

        'find_rooms.index' => 'find-room.title',
    ];

    return array_key_exists($route, $items) ? transFunc($items[$route]) . ' | ' : '';
}

function flashMessage($message = 'success', $redirectPath = null, $status = 'success')
{
    $redirectPath = $redirectPath == null ? back() : redirect($redirectPath);

    return $redirectPath->with([
        $status => $message
    ]);
}