<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Auth gates for: User management
        $this->accessFunc('user_management_access', [1]);

        // Auth gates for: Roles
        $this->accessFunc('role_access', [1]);
        $this->accessFunc('role_create', [1]);
        $this->accessFunc('role_edit', [1]);
        $this->accessFunc('role_view', [1]);
        $this->accessFunc('role_delete', [1]);

        // Auth gates for: Users
        $this->accessFunc('user_access', [1]);
        $this->accessFunc('user_create', [1]);
        $this->accessFunc('user_edit', [1]);
        $this->accessFunc('user_view', [1]);
        $this->accessFunc('user_delete', [1]);

        // Auth gates for: Rooms
        $this->accessFunc('room_access', [1, 2, 3]);
        $this->accessFunc('room_create', [1]);
        $this->accessFunc('room_edit', [1]);
        $this->accessFunc('room_view', [1, 2]);
        $this->accessFunc('room_delete', [1]);

        // Auth gates for: Bookings
        $this->accessFunc('booking_access', [1, 2, 3]);
        $this->accessFunc('booking_create', [1, 2, 3]);
        $this->accessFunc('booking_edit', [1, 2, 3]);
        $this->accessFunc('booking_view', [1, 2, 3]);
        $this->accessFunc('booking_view', [1, 2, 3]);

        // Auth gates for: Find room
        $this->accessFunc('find_room_access', [1, 2, 3]);

        // Auth gates for: departments
        $this->accessFunc('category_access', [1]);
        $this->accessFunc('category_create', [1]);
        $this->accessFunc('category_edit', [1]);
        $this->accessFunc('category_view', [1]);
        $this->accessFunc('category_delete', [1]);

        //admin access
        $this->accessFunc('admin_access', [1]);
    }

    function accessFunc($ability, $roles = [])
    {
        Gate::define($ability, function ($user) use ($roles) {
            return in_array($user->role_id, $roles);
        });
    }
}