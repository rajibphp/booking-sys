<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable = ['student_id', 'batch', 'section'];
    protected $guarded = ['id', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
