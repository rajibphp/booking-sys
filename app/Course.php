<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['code', 'name', 'teacher_id'];

    static function selectList()
    {
        $select = ['' => transFunc()];
        $courses = self::all();
        foreach ($courses as $item) {
            $select[$item->id] = $item->code . " [" . $item->name . "]";
        }
        return $select;
    }

    function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }
}
