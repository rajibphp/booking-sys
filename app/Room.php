<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * Class Room
 *
 * @package App
 * @property string $room_number
 * @property integer $floor
 * @property text $description
 */
class Room extends Model
{
    use SoftDeletes;

    protected $fillable = ['room_number', 'floor', 'description'];

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setFloorAttribute($input)
    {
        $this->attributes['floor'] = $input ? $input : null;
    }

    public function booking()
    {
        return $this->hasMany(Booking::class, 'room_id')->withTrashed()
            ->orderBy('time_from', 'desc');
    }

    static function selectList($build = 0, $where = [])
    {
        $select = ['' => $trans = transFunc()];
        $rooms = Room::where($where);
        if ($build) {
            $rooms = $rooms->get();
            foreach ($rooms as $item)
                $select[$item->id] = $item->room_number . " [" . $item->floor . "]";

        } else {
            return $rooms->pluck('name', 'id')
                ->prepend($trans, '');
        }
        return $select;
    }

    public function avail()
    {
        return $this->hasMany('App\RoomAvail');
    }
}