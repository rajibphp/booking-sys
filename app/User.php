<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Notifications\ResetPassword;
use Hash;
use Illuminate\Support\Facades\Auth;

/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $role
 * @property string $remember_token
 */
class User extends Authenticatable
{
    use Notifiable;
    protected $fillable = ['name', 'email', 'password', 'remember_token', 'role_id', 'department_id'];


    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setRoleIdAttribute($input)
    {
        $this->attributes['role_id'] = $input ? $input : null;
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function detail()
    {
        return $this->hasOne('App\UserDetail');
    }
    public function teacher()
    {
        return $this->hasOne('App\Teacher');
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    static function selectList($dept = 0)
    {
        $user = Auth::user();
        $users = self::where($user->role_id > 1 ? ['id' => $user->id] : []);
        $select = ['' => transFunc()];
        if ($dept) {
            $users = $users->with('department')->get();
            foreach ($users as $item)
                $select[$item->id] = $item->name . " [" . $item->department->name . "]";

        } else {
            return $users->pluck('name', 'id')
                ->prepend($trans, '');
        }
        return $select;
    }

}