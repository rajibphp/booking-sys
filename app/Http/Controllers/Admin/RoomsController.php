<?php

namespace App\Http\Controllers\Admin;

use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRoomsRequest;
use App\Department;
use App\RoomAvail;

class RoomsController extends Controller
{
    private $model = 'Room ';
    private $route = 'rooms';

    /**
     * Display a listing of Room.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows('room_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (!Gate::allows('room_delete')) {
                return abort(401);
            }
            $rooms = Room::onlyTrashed()->get();
        } else {
            $rooms = Room::all();
        }

        return view('admin.rooms.index', compact('rooms'));
    }

    /**
     * Show the form for creating new Room.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('room_create')) {
            return abort(401);
        }

        $departments = Department::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        return view('admin.rooms.create', compact('departments'));
    }

    /**
     * Store a newly created Room in storage.
     *
     * @param \App\Http\Requests\StoreRoomsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoomsRequest $request)
    {
        if (!Gate::allows('room_create')) {
            return abort(401);
        }

        $room = Room::create($request->all());
        $availData = $this->availData($request, $room->id);
        $room->avail()->insert($availData);
        return flashMessage($this->model . transFunc('qa_is_created'), $this->route);
    }

    private function availData($request, $room_id)
    {
        $avails = [];
        if (!empty($days = $request->day)) {
            foreach ($days as $k => $item) {
                $avails[] = [
                    'room_id' => $room_id,
                    'day' => $item,
                    'time_from' => timeFormat('H:i', $request->time_from[$k] ?? ''),
                    'time_to' => timeFormat('H:i', $request->time_to[$k] ?? ''),
                ];
                if (isset($request->avail_id)) {
                    $avails[$k]['id'] = (isset($request->avail_id[$k])) ? $request->avail_id[$k] : null;
                }
            }
        }
        return $avails;
    }

    /**
     * Show the form for editing Room.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Gate::allows('room_edit')) {
            return abort(401);
        }
        $room = Room::findOrFail($id);
        $departments = Department::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        return view('admin.rooms.edit', compact('room', 'departments'));
    }

    /**
     * Update Room in storage.
     *
     * @param \App\Http\Requests\UpdateRoomsRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRoomsRequest $request, $id)
    {
        if (!Gate::allows('room_edit')) {
            return abort(401);
        }

        $room = Room::findOrFail($id);
        $room->update($request->all());

        $availData = $this->availData($request, $id);
        $existIds = RoomAvail::select('id')->where('room_id', $id)->get()->toArray();
        $existIds = array_flatten($existIds);
        $removableIds = array_diff($existIds, $request->avail_id ?? []);
        RoomAvail::whereIn('id', $removableIds)->delete();

        foreach ($availData as $item) {
            try {
                RoomAvail::updateOrCreate(['id' => $item['id']], $item);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }

        return flashMessage($this->model . transFunc('qa_is_updated'), $this->route);
    }

    /**
     * Display Room.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Gate::allows('room_view')) {
            return abort(401);
        }
        $bookings = \App\Booking::where('room_id', $id)->get();

        $room = Room::findOrFail($id);

        return view('admin.rooms.show', compact('room', 'bookings'));
    }

    /**
     * Remove Room from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows('room_delete')) {
            return abort(401);
        }
        $room = Room::findOrFail($id);
        try {
            $room->delete();
        } catch (\Exception $e) {
            return redirectException($e);
        }

        return flashMessage($this->model . transFunc('qa_is_deleted'), $this->route);
    }

    /**
     * Delete all selected Room at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (!Gate::allows('room_delete')) {
            return abort(401);
        }

        if ($ids = $request->input('ids')) {
            try {
                Room::whereIn('id', $ids)->delete();
            } catch (\Exception $e) {
                return redirectException($e);
            }
        }
    }

    /**
     * Restore Room from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (!Gate::allows('room_delete')) {
            return abort(401);
        }
        $room = Room::onlyTrashed()->findOrFail($id);
        $room->restore();

        return redirect()->route('rooms.index');
    }

    /**
     * Permanently delete Room from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (!Gate::allows('room_delete')) {
            return abort(401);
        }
        $room = Room::onlyTrashed()->findOrFail($id);
        try {
            $room->forceDelete();
            RoomAvail::whereIn('room_id', [$id])->delete();
        } catch (\Exception $e) {
            return redirectException($e);
        }

        return redirect()->route('rooms.index');
    }
}