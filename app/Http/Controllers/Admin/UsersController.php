<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Teacher;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;

class UsersController extends Controller
{
    private $model = 'User ';
    private $route = 'users';

    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows('user_access')) {
            return abort(401);
        }

        $users = User::with('department')->get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('user_create')) {
            return abort(401);
        }

        $roles = \App\Role::get()->pluck('title', 'id')->prepend($trans = trans('quickadmin.qa_please_select'), '');
        $departments = \App\Department::get()->pluck('name', 'id')->prepend($trans, '');
        return view('admin.users.create', compact('roles', 'departments'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param \App\Http\Requests\StoreUsersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsersRequest $request)
    {
        if (!Gate::allows('user_create')) {
            return abort(401);
        }
        $user = User::create($request->all());
        if ($user->role_id == 2) {
            $user->detail()->create($request->all());
        } elseif ($user->role_id == 3) {
            $user->teacher()->create($request->all());
        }

        return flashMessage($this->model . transFunc('qa_is_created'), $this->route);
    }


    /**
     * Show the form for editing User.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Gate::allows('user_edit')) {
            return abort(401);
        }

        $roles = \App\Role::get()->pluck('title', 'id')->prepend($trans = trans('quickadmin.qa_please_select'), '');
        $departments = \App\Department::get()->pluck('name', 'id')->prepend($trans, '');
        $user = User::findOrFail($id);

        return view('admin.users.edit', compact('user', 'roles', 'departments'));
    }

    /**
     * Update User in storage.
     *
     * @param \App\Http\Requests\UpdateUsersRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsersRequest $request, $id)
    {
        if (!Gate::allows('user_edit')) {
            return abort(401);
        }
        $user = User::findOrFail($id);
        $user->update($request->all());

        if ($user->role_id == 2) {
            if ($user->detail) {
                $user->detail->update($request->all());
            } else {
                $user->detail()->create($request->all());
            }
        } elseif ($user->role_id == 3) {
            if ($user->teacher) {
                $user->teacher->update($request->all());
            } else {
                $user->teacher()->create($request->all());
            }
        } else {
            $user->detail ? $user->detail()->delete() : '';
        }

        return flashMessage($this->model . transFunc('qa_is_updated'), $this->route);
    }


    /**
     * Display User.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Gate::allows('user_view')) {
            return abort(401);
        }
        $user = User::findOrFail($id);

        return view('admin.users.show', compact('user'));
    }


    /**
     * Remove User from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows('user_delete')) {
            return abort(401);
        }
        $user = User::findOrFail($id);
        try {
            if ($user->role_id == 3) {
                $course = Course::where('teacher_id', $user->teacher->id)->first();
                if ($course) {
                    return flashMessage($this->model . "already used in Course", $this->route, 'warning');
                }
            }

            $user->delete();
            $user->detail()->delete();
            $user->teacher()->delete();
        } catch (\Exception $e) {
            return redirectException($e);
        }

        return flashMessage($this->model . transFunc('qa_is_deleted'), $this->route);
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (!Gate::allows('user_delete')) {
            return abort(401);
        }
        if ($ids = $request->input('ids')) {
            try {

//                User::whereIn('id', $ids)->delete();
//                UserDetail::whereIn('user_id', $ids)->delete();
//                Teacher::whereIn('user_id', $ids)->delete();
            } catch (\Exception $e) {
                return redirectException($e);
            }
        }
    }

}