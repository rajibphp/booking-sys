<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

use App\Teacher;
use App\Http\Requests\Admin\StoreTeachersRequest;

class TeachersController extends Controller
{
    private $model = 'Teacher ';
    private $route = 'teachers';

    public function index()
    {
        $teachers = Teacher::all();
        return view('admin.teachers.index', compact('teachers'));
    }

    public function create()
    {
        //show template
        return view('admin.teachers.create');
    }

    public function store(StoreTeachersRequest $request)
    {
        if (!Gate::allows('category_create')) {
            return abort(401);
        }

        Teacher::updateOrCreate(['desig' => $request->desig, 'name' => $request->name]);
        return flashMessage($this->model . transFunc('qa_is_created'), $this->route);

    }

    /**
     * Show the form for editing.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Gate::allows('category_edit')) {
            return abort(401);
        }
        $teacher = Teacher::findOrFail($id);

        return view('admin.teachers.edit', compact('teacher'));
    }

    /**
     * Update in storage.
     *
     * @param \App\Http\Requests\UpdateCountriesRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTeachersRequest $request, $id)
    {
        if (!Gate::allows('category_edit')) {
            return abort(401);
        }
        $teacher = Teacher::findOrFail($id);
        $teacher->update($request->all());

        return flashMessage($this->model . transFunc('qa_is_updated'), $this->route);
    }

    /**
     * Display.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Gate::allows('category_view')) {
            return abort(401);
        }
        $teacher = Teacher::findOrFail($id);

        return view('admin.teachers.show', compact('teacher'));
    }

    /**
     * Remove from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows('category_delete')) {
            return abort(401);
        }
        $teacher = Teacher::findOrFail($id);
        try {
            $teacher->delete();
        } catch (\Exception $e) {
            return redirectException($e);
        }

        return flashMessage($this->model . transFunc('qa_is_deleted'), $this->route);
    }

    /**
     * Delete all selected at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (!Gate::allows('admin_access')) {
            return abort(401);
        }
        if ($ids = $request->input('ids')) {
            try {
                Teacher::whereIn('id', $ids)->delete();
            } catch (\Exception $e) {
                return redirectException($e);
            }
        }
    }

}