<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use App\Course;
use App\Room;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBookingsRequest;
use App\Http\Requests\Admin\UpdateBookingsRequest;

class BookingsController extends Controller
{
    private $unavailMsg = 'Sorry! the classroom is not available for your defined datetime';
    private $model = 'Booking ';
    private $route = 'bookings';

    /**
     * Display a listing of Booking.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows('booking_access')) {
            return abort(401);
        }

        if (request('show_deleted') == 1) {
            if (!Gate::allows('admin_access')) {
                return abort(401);
            }
            $bookings = Booking::onlyTrashed()->get();
        } else {
            $bookings = Booking::with('user', 'course');
            if (!Gate::allows('admin_access')) {
                $bookings->where('user_id', Auth::user()->id);
            }
            $bookings = $bookings->orderBy('time_from', 'DESC')->get();
        }

        return view('admin.bookings.index', compact('bookings'));
    }

    /**
     * Show the form for creating new Booking.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('booking_create')) {
            return abort(401);
        }

        $data = [
            'users' => User::selectList(1),
            'courses' => Course::selectList(),
            'rooms' => Room::selectList(1, ['id' => $_GET['room_id'] ?? ''])
        ];
        return view('admin.bookings.create', $data);
    }

    /**
     * Store a newly created Booking in storage.
     *
     * @param \App\Http\Requests\StoreBookingsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookingsRequest $request)
    {
        if (!Gate::allows('booking_create')) {
            return abort(401);
        }

        if ($this->availability($request)) {
            return redirect()->back()->withInput()->with('warning', $this->unavailMsg);
        }
        Booking::create($request->all());
        return flashMessage($this->model . transFunc('qa_is_created'), $this->route);
    }

    /**
     * checking if the availability of Booking in storage.
     *
     * @param \App\Http\Requests\StoreBookingsRequest $request
     * @return \Illuminate\Http\Response
     */
    private function availability($request, $id = null)
    {
        $time_from = $request->input('time_from');
        $time_to = $request->input('time_to');
        $rooms = null;
        $rooms = Room::with(['booking' => function ($q) use ($time_from, $time_to) {
            $q->where(function ($q2) use ($time_from, $time_to) {
                $q2->whereBetween('time_from', [convertTime($time_from, 24), convertTime($time_to, 24)])
                    ->orWhereBetween('time_to', [convertTime($time_from, 24), convertTime($time_to, 24)]);
            });
        }])
            ->where('id', $request->room_id)
            ->first();

        if (count($bookings = $rooms->booking) > 0) {
            if ($id) {
                foreach ($bookings as $item) {
                    if ($item->id == $id && $item->time_from == convertTime($time_from) && $item->time_to == convertTime($time_to)) {
                        return false;
                    }
                    if ($item->id == $id) {
                        return false;
                    }
                }
            }
            return true;
        }

        return false;
    }

    /**
     * Show the form for editing Booking.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Gate::allows('booking_edit')) {
            return abort(401);
        }
        $data = [
            'users' => User::selectList(1),
            'courses' => Course::selectList(),
            'rooms' => Room::selectList(1),
            'booking' => Booking::findOrFail($id)
        ];
        return view('admin.bookings.edit', $data);
    }

    /**
     * Update Booking in storage.
     *
     * @param \App\Http\Requests\UpdateBookingsRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBookingsRequest $request, $id)
    {
        if (!Gate::allows('booking_edit')) {
            return abort(401);
        }
        if ($this->availability($request, $id)) {
            return redirect()->back()->withInput()->with('warning', $this->unavailMsg);
        }

        $booking = Booking::findOrFail($id);
        $booking->update($request->all());

        return flashMessage($this->model . transFunc('qa_is_updated'), $this->route);
    }


    /**
     * Display Booking.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Gate::allows('booking_view')) {
            return abort(401);
        }
        $booking = Booking::findOrFail($id);

        return view('admin.bookings.show', compact('booking'));
    }


    /**
     * Remove Booking from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows('admin_access')) {
            return abort(401);
        }
        $booking = Booking::findOrFail($id);
        $booking->delete();

        return flashMessage($this->model . transFunc('qa_is_deleted'), $this->route);
    }

    /**
     * Delete all selected Booking at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (!Gate::allows('admin_access')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            Booking::whereIn('id', $request->input('ids'))->delete();
        }
    }


    /**
     * Restore Booking from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (!Gate::allows('admin_access')) {
            return abort(401);
        }
        $booking = Booking::onlyTrashed()->findOrFail($id);
        $booking->restore();

        return redirect()->route('bookings.index');
    }

    /**
     * Permanently delete Booking from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (!Gate::allows('admin_access')) {
            return abort(401);
        }
        $booking = Booking::onlyTrashed()->findOrFail($id);
        $booking->forceDelete();

        return redirect()->route('bookings.index');
    }

    /**
     * update Booking Status from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request, $id)
    {
        if (!Gate::allows('admin_access')) {
            return abort(401);
        }
        $booking = Booking::findOrFail($id);
        $booking->update($request->all());
        $room = $booking->room->room_number . " [" . $booking->room->floor . "]";
        return redirect()->route('bookings.index')
            ->with('success', "Booking for Room $room has been " . status()[$booking->status]);
    }
}