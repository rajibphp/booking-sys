<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

use App\Ctegory;
use App\Department;
use App\Http\Requests\Admin\StoreDepartmentsRequest;
class DepartmentsController extends Controller
{
    private $model = 'Department ';
    private $route = 'departments';

    public function index()
    {
        $departments = Department::all();
        return view('admin.departments.index', compact('departments'));
    }

    public function create()
    {
        //show template
        return view('admin.departments.create');
    }

    public function store(StoreDepartmentsRequest $request)
    {
        if (!Gate::allows('category_create')) {
            return abort(401);
        }

        Department::create([
            'name' => $request->name
        ]);
        return flashMessage($this->model . transFunc('qa_is_created'), $this->route);

    }

    /**
     * Show the form for editing category.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Gate::allows('category_edit')) {
            return abort(401);
        }
        $category = Department::findOrFail($id);

        return view('admin.departments.edit', compact('category'));
    }

    /**
     * Update category in storage.
     *
     * @param \App\Http\Requests\UpdateCountriesRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDepartmentsRequest $request, $id)
    {
        if (!Gate::allows('category_edit')) {
            return abort(401);
        }
        $category = Department::findOrFail($id);
        $category->update($request->all());

        return flashMessage($this->model . transFunc('qa_is_updated'), $this->route);
    }

    /**
     * Display User.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Gate::allows('category_view')) {
            return abort(401);
        }
        $category = Department::findOrFail($id);

        return view('admin.departments.show', compact('category'));
    }

    /**
     * Remove Booking from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows('category_delete')) {
            return abort(401);
        }
        $booking = Department::findOrFail($id);
        try {
            $booking->delete();
        } catch (\Exception $e) {
            return redirectException($e);
        }

        return flashMessage($this->model . transFunc('qa_is_deleted'), $this->route);
    }

    /**
     * Delete all selected Category at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (!Gate::allows('admin_access')) {
            return abort(401);
        }

        if ($ids = $request->input('ids')) {
            try {
                Department::whereIn('id', $ids)->delete();
            } catch (\Exception $e) {
                return redirectException($e);
            }
        }
    }


    /**
     * Restore Category from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (!Gate::allows('admin_access')) {
            return abort(401);
        }
        $booking = Department::onlyTrashed()->findOrFail($id);
        $booking->restore();

        return redirect()->route('bookings.index');
    }

    /**
     * Permanently delete Category from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (!Gate::allows('admin_access')) {
            return abort(401);
        }
        $booking = Department::onlyTrashed()->findOrFail($id);
        $booking->forceDelete();

        return redirect()->route('bookings.index');
    }

}