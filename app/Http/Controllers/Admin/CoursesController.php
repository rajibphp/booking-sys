<?php

namespace App\Http\Controllers\Admin;

use App\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

use App\Course;
use App\Http\Requests\Admin\StoreCoursesRequest;

class CoursesController extends Controller
{
    private $model = 'Course ';
    private $route = 'courses';

    public function index()
    {
        $courses = Course::with('teacher')->get();
        return view('admin.courses.index', compact('courses'));
    }

    public function create()
    {
        //show template
        $teachers = Teacher::selectList();
        return view('admin.courses.create', compact('teachers'));
    }

    public function store(StoreCoursesRequest $request)
    {
        if (!Gate::allows('category_create')) {
            return abort(401);
        }

        Course::create($request->all());
        return flashMessage($this->model . transFunc('qa_is_created'), $this->route);

    }

    /**
     * Show the form for editing.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Gate::allows('category_edit')) {
            return abort(401);
        }
        $teachers = Teacher::selectList();
        $course = Course::findOrFail($id);

        return view('admin.courses.edit', compact('course', 'teachers'));
    }

    /**
     * Update in storage.
     *
     * @param \App\Http\Requests\UpdateCountriesRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCoursesRequest $request, $id)
    {
        if (!Gate::allows('category_edit')) {
            return abort(401);
        }
        $course = Course::findOrFail($id);
        $course->update($request->all());

        return flashMessage($this->model . transFunc('qa_is_updated'), $this->route);
    }

    /**
     * Display.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Gate::allows('category_view')) {
            return abort(401);
        }
        $course = Course::findOrFail($id);

        return view('admin.courses.show', compact('course'));
    }

    /**
     * Remove from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows('category_delete')) {
            return abort(401);
        }
        $course = Course::findOrFail($id);
        try {
            $course->delete();
        } catch (\Exception $e) {
            return redirectException($e);
        }

        return flashMessage($this->model . transFunc('qa_is_deleted'), $this->route);
    }

    /**
     * Delete all selected at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (!Gate::allows('admin_access')) {
            return abort(401);
        }

        if ($ids = $request->input('ids')) {
            try {
                Course::whereIn('id', $ids)->delete();
            } catch (\Exception $e) {
                return redirectException($e);
            }
        }
    }

}