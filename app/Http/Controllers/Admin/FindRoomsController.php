<?php

namespace App\Http\Controllers\Admin;

use App\Room;
use App\RoomAvail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

class FindRoomsController extends Controller
{
    public function index(Request $request)
    {
        if (!Gate::allows('find_room_access')) {
            return abort(401);
        }
        $time_from = $request->input('time_from');
        $time_to = $request->input('time_to');
        $rooms = null;

        if ($request->isMethod('POST')) {

            $dayFrom = array_search(timeFormat('l', $time_from), days());
            $dayTo = array_search(timeFormat('l', $time_to), days());
            $dayTimeFrom = timeFormat('H:i', $time_from);
            $dayTimeTo = timeFormat('H:i', $time_to);

            $avail = RoomAvail::whereBetween('day', [$dayFrom, $dayTo])
                ->whereBetween('time_from', [$dayTimeFrom, $dayTimeTo])
                ->whereBetween('time_to', [$dayTimeFrom, $dayTimeTo])
                ->pluck('room_id')->toArray();

            $rooms = Room::with(['booking' => function ($q) use ($time_from, $time_to) {
                $q->where(function ($q2) use ($time_from, $time_to) {
                    $q2->orderBy('time_from', 'desc');
                    $q2->whereBetween('time_from', [convertTime($time_from, 24), convertTime($time_to, 24)])
                        ->orWhereBetween('time_to', [convertTime($time_from, 24), convertTime($time_to, 24)]);

                });
            }])->whereIn('id', $avail)->get();
        }

        return view('admin.find_rooms.index', compact('rooms', 'time_from', 'time_to'));
    }

}