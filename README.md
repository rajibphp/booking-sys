# Classroom Booking
![Classroom_Booking screenshot](doc/classroom-booking-demo.PNG)

## About Classroom Booking
It is a software project for booking classroom for a particular day. Classroom Booking is all generated for some custom code for Room Search.

## Contributing

Thank you for considering contributing to the Classroom Booking.


## Credits
<p align="center"><a href="https://facebook.com/rajibhosssain" target="_blank">Rajib Hossain</a></p>

## License

The Classroom booking is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
